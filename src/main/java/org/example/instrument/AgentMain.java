package org.example.instrument;

import cn.hutool.core.util.RandomUtil;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

public class AgentMain {

    public static void agentmain(String agentArgs, Instrumentation inst)  throws ClassNotFoundException, UnmodifiableClassException, InterruptedException {
        inst.addTransformer(new PlayClassFileTransformer(), true);
        inst.retransformClasses(RandomUtil.class);
        System.out.println("Agent Main Done"); 
    }

}