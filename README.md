### 如何attach

```java
public static void main(String[] args) throws InterruptedException, IOException, AgentLoadException, AgentInitializationException, AttachNotSupportedException {
    VirtualMachine vmObj = null;
    try {
        vmObj = VirtualMachine.attach("47834");
        if (vmObj != null) {
            vmObj.loadAgent("<jar path>/instrument-play-1.0-SNAPSHOT-jar-with-dependencies.jar", null);
        }
    } finally {
        if (null != vmObj) {
            vmObj.detach();
        }
    }
}
```

docs 目录下的RandomUtil.class是经过串改的字节码文件，simpleUUID永远返回hello